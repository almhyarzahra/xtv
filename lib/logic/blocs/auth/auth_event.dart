part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class AuthRegister extends AuthEvent {
  final String username;
  final String password;
  final List<String> servers;

  AuthRegister(this.username, this.password, this.servers);
}

class AuthGetUser extends AuthEvent {}

class AuthLogOut extends AuthEvent {}
