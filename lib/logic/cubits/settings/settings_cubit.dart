import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import '../../../repository/api/api.dart';
import '../../../repository/models/server_dns.dart';

part 'settings_state.dart';

const platform = MethodChannel('main_activity_channel');

class SettingsCubit extends Cubit<SettingsState> {
  final AuthApi authApi;
  SettingsCubit(this.authApi) : super(SettingsState(setting: "null"));

  void getSettingsCode() async {
    try {
      String data = await platform.invokeMethod('getData');
      debugPrint("DATA: $data");

      emit(SettingsState(setting: data));
    } catch (e) {
      debugPrint("Error: $e");
    }
  }
}
