class ServersDns {
  final String? name;
  final String? image;
  final List<String>? servers;

  ServersDns({this.name, this.image, this.servers});

  factory ServersDns.fromJson(Map<String, dynamic> json) {
    return ServersDns(
      name: json["name"],
      image: json["image"],
      servers: (json["servers"] as List).map((e) => e as String).toList(),
    );
  }
//
}
