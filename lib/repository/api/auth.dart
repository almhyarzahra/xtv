part of 'api.dart';

class AuthApi {
  Future<UserModel?> registerUser(
    String username,
    String password,
    String link,
    String name,
  ) async {
    try {
      Response<String> response = await _dio
          .get("$link/player_api.php?username=$username&password=$password");

      if (response.statusCode == 200) {
        var json = jsonDecode(response.data ?? "");
        final user = UserModel.fromJson(json, link);

        if (user.userInfo!.auth != "0") {
          //save to locale
          await LocaleApi.saveUser(user);
          return user;
        }

        return null;
      } else {
        return null;
      }
    } catch (e) {
      debugPrint("Error: $e");
      return null;
    }
  }

  ///Firebase

  static Future<List<ServersDns>> getServers() async {
    try {
      final data = await _firestore
          .collection("servers")
          .orderBy("date", descending: false)
          .get();

      debugPrint("SERVERS: ${data.size}");

      return data.docs.map((e) => ServersDns.fromJson(e.data())).toList();
    } catch (e) {
      debugPrint("Firebase Error: $e");
      return [];
    }
  }
}
