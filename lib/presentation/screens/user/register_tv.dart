part of '../screens.dart';

class RegisterUserTv extends StatefulWidget {
  const RegisterUserTv({Key? key}) : super(key: key);

  @override
  State<RegisterUserTv> createState() => _RegisterUserTvState();
}

class _RegisterUserTvState extends State<RegisterUserTv> {
  late ServersDns serversDns;
  final _username = TextEditingController(text: "Zizi");
  final _password = TextEditingController(text: "123");

  int indexTab = 0;

  final FocusNode focusNode1 = FocusNode();
  final FocusNode focusNode2 = FocusNode();
  final FocusNode _remoteFocus = FocusNode();

  _onKey(RawKeyEvent event) {
    debugPrint("EVENT");
    if (event.isKeyPressed(LogicalKeyboardKey.arrowDown)) {
      debugPrint('downd');

      if (indexTab == 0) {
        indexTab = 1;
      } else if (indexTab == 1) {
        indexTab = 2;
      }
    } else if (event.isKeyPressed(LogicalKeyboardKey.arrowUp)) {
      debugPrint('up');
      if (indexTab == 1) {
        indexTab = 0;
      } else if (indexTab == 2) {
        indexTab = 1;
      }
    } else if (event.isKeyPressed(LogicalKeyboardKey.select)) {
      debugPrint("enter");

      if (indexTab == 0) {
        focusNode1.requestFocus();
      } else if (indexTab == 1) {
        focusNode2.requestFocus();
      } else if (indexTab == 2) {
        _remoteFocus.requestFocus();
        debugPrint("Login");
        _login();
      }
    }
    setState(() {});
  }

  @override
  void initState() {
    serversDns = Get.arguments as ServersDns;
    super.initState();
    focusNode1.requestFocus();
  }

  @override
  void dispose() {
    _username.dispose();
    _password.dispose();
    focusNode1.dispose();
    focusNode2.dispose();

    super.dispose();
  }

  _login() {
    if (_username.text.isNotEmpty && _password.text.isNotEmpty) {
      context.read<AuthBloc>().add(AuthRegister(
            _username.text,
            _password.text,
            serversDns.servers ?? [],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("WIDGT: ${MediaQuery.of(context).size.width}");

    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, stateAuth) {
        if (stateAuth is AuthFailed) {
          showWarningToast(
            context,
            'Login failed.',
            'Please check your IPTV credentials and try again.',
          );
        } else if (stateAuth is AuthSuccess) {
          context.read<LiveCatyBloc>().add(GetLiveCategories());
          context.read<MovieCatyBloc>().add(GetMovieCategories());
          context.read<SeriesCatyBloc>().add(GetSeriesCategories());
          Get.offAndToNamed(screenWelcome);
        }
      },
      builder: (context, stateAuth) {
        bool isLoading = stateAuth is AuthLoading;

        return BlocBuilder<SettingsCubit, SettingsState>(
          builder: (context, stateSetting) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              body: RawKeyboardListener(
                focusNode: _remoteFocus,
                onKey: (event) {
                  _onKey(event);
                },
                child: Ink(
                  width: getSize(context).width,
                  height: getSize(context).height,
                  decoration: kDecorBackground,
                  child: Stack(
                    children: [
                      const Opacity(
                        opacity: .1,
                        child: IntroImageAnimated(),
                      ),
                      SizedBox(
                        width: getSize(context).width,
                        height: getSize(context).height,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Hero(
                                  tag: "x",
                                  child: Image(
                                    image: const AssetImage(kIconSplash),
                                    width: 42.sp,
                                    height: 42.sp,
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  'Watch Live Tv, movies, Tv shows,\nentertainment channels and much\nmore on $kAppName',
                                  textAlign: TextAlign.center,
                                  style: Get.textTheme.bodyLarge!.copyWith(
                                    color: Colors.white,
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Center(
                                  child: Text(
                                    'Enter your Login Details',
                                    textAlign: TextAlign.center,
                                    style: Get.textTheme.bodyLarge!.copyWith(
                                      color: Colors.white,
                                      fontSize: 20.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 15),
                                CardInputTv(
                                  label: "username",
                                  controller: _username,
                                  icon: FontAwesomeIcons.solidUser,
                                  focusNode: focusNode1,
                                  isEnabled: indexTab == 0,
                                  isFocused: indexTab == 0,
                                  onEditingComplete: isTv(context)
                                      ? null
                                      : () async {
                                          debugPrint("finish: username");
                                          setState(() {
                                            indexTab = 1;
                                            focusNode2.requestFocus();
                                          });
                                        },
                                  onTap: () {
                                    setState(() {
                                      indexTab = 0;
                                    });
                                  },
                                ),
                                const SizedBox(height: 10),
                                CardInputTv(
                                  label: "password",
                                  controller: _password,
                                  icon: FontAwesomeIcons.lock,
                                  focusNode: focusNode2,
                                  isEnabled: indexTab == 1,
                                  isFocused: indexTab == 1,
                                  onTap: () {
                                    setState(() {
                                      indexTab = 1;
                                    });
                                  },
                                ),
                                const SizedBox(height: 15),
                                isLoading
                                    ? LoadingAnimationWidget.staggeredDotsWave(
                                        color: Colors.white,
                                        size: 40,
                                      )
                                    : SizedBox(
                                        width: 140,
                                        height: 43,
                                        child: CardButtonWatchMovie(
                                          title: "Login",
                                          isSelected: indexTab == 2,
                                          onTap: () {
                                            _login();
                                          },
                                        ),
                                      )
                              ],
                            ),
                          ],
                        ),
                      ),

                      ///Pricavy
                      /* Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          FontAwesomeIcons.solidCircle,
                          color: Colors.white70,
                          size: 12.sp,
                        ),
                        const SizedBox(width: 8),
                        Text(
                          'By registering, you are agreeing to our ',
                          style: Get.textTheme.bodyMedium!.copyWith(
                            color: Colors.white70,
                          ),
                        ),
                        InkWell(
                          onTap: () async {
                            var url = Uri.parse(kPrivacy);
                            await launchUrl(url,
                                mode: LaunchMode.externalApplication);
                          },
                          child: Text(
                            'privacy policy.',
                            style: Get.textTheme.bodyMedium!.copyWith(
                              color: kColorPrimary.withOpacity(.70),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),*/
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}

class CardInputTv extends StatelessWidget {
  const CardInputTv(
      {Key? key,
      this.controller,
      required this.label,
      required this.icon,
      required this.focusNode,
      required this.onTap,
      this.onEditingComplete,
      required this.isFocused,
      required this.isEnabled})
      : super(key: key);
  final TextEditingController? controller;
  final FocusNode focusNode;
  final String label;
  final IconData icon;
  final Function() onTap;
  final Function()? onEditingComplete;

  final bool isFocused, isEnabled;

  @override
  Widget build(BuildContext context) {
    final style = Get.textTheme.bodyMedium!.copyWith(
      color: Colors.black,
      fontSize: 14.sp,
      fontWeight: FontWeight.bold,
    );

    return InkWell(
      onTap: onTap,
      child: Container(
        width: getSize(context).width * .30,
        height: 50,
        constraints: const BoxConstraints(maxWidth: 300),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: isFocused ? kColorPrimary : Colors.transparent,
            width: 3,
          ),
        ),
        padding: const EdgeInsets.only(left: 10),
        child: TextField(
          controller: controller,
          enabled: isEnabled,
          focusNode: focusNode,
          textInputAction: TextInputAction.next,
          onEditingComplete: onEditingComplete,
          decoration: InputDecoration(
            hintText: label,
            hintStyle: Get.textTheme.bodyMedium!.copyWith(color: Colors.grey),
            suffixIcon: Icon(
              icon,
              size: 18,
              color: kColorPrimary,
            ),
            border: InputBorder.none,
          ),
          cursorColor: kColorPrimary,
          style: style,
        ),
      ),
    );
  }
}
