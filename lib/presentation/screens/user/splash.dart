part of '../screens.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  goScreen(String screen) {
    Future.delayed(const Duration(seconds: 1)).then((value) {
      Get.offAndToNamed(screen);
    });
  }

  @override
  void initState() {
    context.read<SettingsCubit>().getSettingsCode();
    context.read<AuthBloc>().add(AuthGetUser());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // debugPrint("width: ${MediaQuery.of(context).size.width}");
    return Scaffold(
      body: OrientationBuilder(builder: (context, orientation) {
        // bool isPortrait = orientation == Orientation.portrait;

        return BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is AuthSuccess) {
              context.read<LiveCatyBloc>().add(GetLiveCategories());
              context.read<MovieCatyBloc>().add(GetMovieCategories());
              context.read<SeriesCatyBloc>().add(GetSeriesCategories());
              goScreen(screenWelcome);
            } else if (state is AuthFailed) {
              goScreen(screenIntro);
              /* if (UniversalPlatform.isDesktopOrWeb) {
                goScreen(screenRegisterTv);
              } else {
                goScreen(screenIntro);
              }*/
            }
          },
          child: const LoadingWidgt(),
        );
      }),
    );
  }
}
