part of '../screens.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  int? isHovered;

  Future<List<ServersDns>> futureDns = AuthApi.getServers();

  @override
  Widget build(BuildContext context) {
    // context.read<SettingsCubit>().getDns();
    return Scaffold(
      body: Ink(
        width: getSize(context).width,
        height: getSize(context).height,
        decoration: kDecorBackground,
        child: Stack(
          children: [
            SizedBox(
              height: getSize(context).height * .28,
              child: const Opacity(opacity: .2, child: IntroImageAnimated()),
            ),
            Column(
              children: [
                Container(
                  constraints: const BoxConstraints(maxHeight: 100),
                  height: getSize(context).height * .20,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20)),
                  child: Image(
                    fit: BoxFit.contain,
                    width: getSize(context).width * .78,
                    image: const AssetImage(kImageLogo),
                  ),
                ),
                Expanded(
                  child: FutureBuilder<List<ServersDns>>(
                    future: futureDns,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Lottie.asset("assets/images/loading.json");
                      } else if (snapshot.hasData) {
                        if (!snapshot.data!.contains(ServersDns(
                            name: "beout ttv",
                            image: "",
                            servers: ["http://beouttv.xyz:80"]))) {
                          snapshot.data!.add(ServersDns(
                              name: "beout ttv",
                              image: "",
                              servers: ["http://beouttv.xyz:80"]));
                        }

                        // snapshot.data!.add(ServersDns(
                        //     name: "beout ttv",
                        //     image: "",
                        //     servers: ["http://beouttv.xyz:80"]));
                        final server = Set.from(snapshot.data!);
                        final servers = server.toList();
                        // if (!servers.contains(ServersDns(
                        //     name: "beout ttv",
                        //     image: "",
                        //     servers: ["http://beouttv.xyz:80"]))) {
                        //   servers.add(ServersDns(
                        //       name: "beout ttv",
                        //       image: "",
                        //       servers: ["http://beouttv.xyz:80"]));
                        // }

                        return GridView.builder(
                          itemCount: servers.length,
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            childAspectRatio: 1.2,
                          ),
                          itemBuilder: (_, i) {
                            final serv = servers[i];
                            return CardServerItem(
                              image: serv.image ?? "",
                              title: serv.name ?? "",
                              onTap: () {
                                if (isTv(context)) {
                                  Get.toNamed(
                                    screenRegisterTv,
                                    arguments: serv,
                                  );
                                } else {
                                  Get.toNamed(
                                    screenRegister,
                                    arguments: serv,
                                  );
                                }
                              },
                              isHover: isHovered == i,
                              onHover: (bool value) {
                                setState(() {
                                  isHovered = i;
                                });
                              },
                            );
                          },
                        );
                      }

                      return const Center(
                        child: Text('Could not load Servers'),
                      );
                    },
                  ),
                ),
                /*CardTallButton(
                  label: "Server 1",
                  onTap: () => Get.toNamed(screenRegister,parameters: {
                    "dns": "hello",
                  }),
                )*/
              ],
            ),
          ],
        ),
      ),
    );
  }
}
