part of '../screens.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late ServersDns serversDns;
  final _username = TextEditingController();
  final _password = TextEditingController();

  @override
  void initState() {
    serversDns = Get.arguments as ServersDns;

    super.initState();
  }

  @override
  void dispose() {
    _username.dispose();
    _password.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final style = Get.textTheme.bodyMedium!.copyWith(
      color: Colors.white,
      fontSize: 16.sp,
      fontWeight: FontWeight.bold,
    );

    return Scaffold(
      body: Ink(
        width: getSize(context).width,
        height: getSize(context).height,
        decoration: kDecorBackground,
        child: SafeArea(
          child: BlocConsumer<AuthBloc, AuthState>(
            listener: (context, state) {
              if (state is AuthFailed) {
                CustomToast.showMessage(
                    message: "check your IPTV credentials and try again.",
                    messageType: MessageType.INFO);
                //showWarningToast(
                //   context,
                //   'Login failed.',
                //   'Please check your IPTV credentials and try again.',
                // );
              } else if (state is AuthSuccess) {
                context.read<LiveCatyBloc>().add(GetLiveCategories());
                context.read<MovieCatyBloc>().add(GetMovieCategories());
                context.read<SeriesCatyBloc>().add(GetSeriesCategories());
                Get.offAndToNamed(screenWelcome);
              }
            },
            builder: (context, state) {
              final isLoading = state is AuthLoading || state is AuthSuccess;

              return IgnorePointer(
                ignoring: isLoading,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: () => Get.back(),
                            icon: const Icon(
                              FontAwesomeIcons.chevronLeft,
                              color: Colors.white,
                            )),
                      ],
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Image.asset(
                                    kIconSplash,
                                    width: 120,
                                    height: 120,
                                    //  color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 15),
                            Text(
                              'SignIn to discover all movies & tv shows & lives tv,\nand enjoy our features.',
                              textAlign: TextAlign.center,
                              style: Get.textTheme.bodyLarge!.copyWith(
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(height: 15),
                            TextField(
                              controller: _username,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(screenWidth(10)))),
                                hintText: " User name",
                                hintStyle: Get.textTheme.bodyMedium!.copyWith(
                                  color: Colors.grey,
                                ),
                                suffixIcon: const Icon(
                                  FontAwesomeIcons.solidUser,
                                  size: 18,
                                  color: kColorPrimary,
                                ),
                              ),
                              style: style,
                            ),
                            const SizedBox(height: 15),
                            TextField(
                              controller: _password,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(screenWidth(10)))),
                                hintText: "Password",
                                hintStyle: Get.textTheme.bodyMedium!.copyWith(
                                  color: Colors.grey,
                                ),
                                suffixIcon: const Icon(
                                  FontAwesomeIcons.lock,
                                  size: 18,
                                  color: kColorPrimary,
                                ),
                              ),
                              style: style,
                            ),
                            const SizedBox(height: 15),
                            // Align(
                            //   alignment: Alignment.centerLeft,
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     mainAxisSize: MainAxisSize.min,
                            //     children: [
                            //       Icon(
                            //         FontAwesomeIcons.solidCircle,
                            //         color: Colors.white70,
                            //         size: 12.sp,
                            //       ),
                            //       const SizedBox(width: 8),
                            //       Text(
                            //         'By registering, you are agreeing to our ',
                            //         style: Get.textTheme.bodyMedium!.copyWith(
                            //           color: Colors.white70,
                            //         ),
                            //       ),
                            //       InkWell(
                            //         onTap: () async {
                            //           var url = Uri.parse(kPrivacy);
                            //           await launchUrl(url,
                            //               mode: LaunchMode.externalApplication);
                            //         },
                            //         child: Text(
                            //           'privacy policy.',
                            //           style: Get.textTheme.bodyMedium!.copyWith(
                            //             color: kColorPrimary.withOpacity(.70),
                            //             fontWeight: FontWeight.bold,
                            //           ),
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: CardTallButton(
                        label: "Login",
                        isLoading: isLoading,
                        onTap: () {
                          //Get.toNamed(screenDownload)

                          if (_username.text.isNotEmpty &&
                              _password.text.isNotEmpty) {
                            context.read<AuthBloc>().add(AuthRegister(
                                  _username.text,
                                  _password.text,
                                  serversDns.servers!,
                                ));
                          }
                        },
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
