part of '../screens.dart';

class LiveChannelsScreen extends StatefulWidget {
  const LiveChannelsScreen({Key? key}) : super(key: key);

  @override
  State<LiveChannelsScreen> createState() => _LiveChannelsScreenState();
}

class _LiveChannelsScreenState extends State<LiveChannelsScreen> {
  VlcPlayerController? _videoPlayerController;
  int? selectedVideo;
  ChannelLive? channelLive;
  CategoryModel? selectedCaty;

  double lastPosition = 0.0;
  String? selectedStreamIdEpg;
  late Timer _timer;
  bool showDrawer = true;

  Future<List<ChannelLive>>? featureChannels;

  @override
  void initState() {
    super.initState();
    /* _timer = Timer.periodic(const Duration(seconds: 20), (timer) {
      if (showDrawer && selectedVideo != null) {
        setState(() {
          showDrawer = false;
        });
      }
    });*/
  }

  @override
  void dispose() async {
    //  _timer.cancel();
    super.dispose();
    if (_videoPlayerController != null) {
      await _videoPlayerController!.stopRendererScanning();
      await _videoPlayerController!.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kColorPrimary.withOpacity(0.5),
      body: WillPopScope(
        onWillPop: () async {
          setState(() {
            showDrawer = !showDrawer;
          });

          return false;
        },
        child: BlocBuilder<LiveCatyBloc, LiveCatyState>(
          builder: (context, stateCaty) {
            if (stateCaty is LiveCatySuccess) {
              final categories = stateCaty.categories;

              return BlocBuilder<AuthBloc, AuthState>(
                builder: (context, stateAuth) {
                  if (stateAuth is AuthSuccess) {
                    final userAuth = stateAuth.user;

                    return Stack(
                      children: [
                        if (selectedVideo != null &&
                            _videoPlayerController != null)
                          SizedBox(
                            width: 100.w,
                            height: 100.h,
                            child: VlcPlayer(
                              controller: _videoPlayerController!,
                              aspectRatio: 16 / 9,
                              placeholder: const Center(
                                  child: CircularProgressIndicator()),
                            ),
                          ),

                        ///SHow Drawer or Hide
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              showDrawer = !showDrawer;
                            });
                          },
                          child: Container(
                            width: 100.w,
                            height: 100.w,
                            color: Colors.transparent,
                          ),
                        ),
                        if (showDrawer)
                          SizedBox(
                            width: 40.w,
                            height: 100.h,
                            child: Material(
                              color: Colors.black54,
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    child: Row(
                                      // mainAxisAlignment:
                                      //     MainAxisAlignment.spaceBetween,
                                      children: [
                                        IconButton(
                                          onPressed: () => Get.back(),
                                          focusColor: kColorPrimaryDark,
                                          icon: const Icon(Icons.arrow_back),
                                        ),
                                        Text(
                                          channelLive == null
                                              ? "Categories"
                                              : channelLive!.name ??
                                                  "Categories",
                                          style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.sp,
                                          ),
                                        ),
                                        channelLive == null
                                            ? const SizedBox()
                                            : BlocBuilder<FavoritesCubit,
                                                FavoritesState>(
                                                builder: (context, state) {
                                                  final favorites = state.lives;

                                                  final isLiked = favorites
                                                      .where((element) =>
                                                          element.streamId ==
                                                          channelLive!.streamId)
                                                      .toList()
                                                      .isNotEmpty;

                                                  return IconButton(
                                                    onPressed: () {
                                                      context
                                                          .read<
                                                              FavoritesCubit>()
                                                          .addLive(channelLive,
                                                              isAdd: !isLiked);
                                                    },
                                                    focusColor:
                                                        kColorPrimaryDark,
                                                    icon: Icon(
                                                      isLiked
                                                          ? FontAwesomeIcons
                                                              .solidHeart
                                                          : FontAwesomeIcons
                                                              .heart,
                                                      size: 17,
                                                    ),
                                                  );
                                                },
                                              ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                      itemCount: categories.length,
                                      itemBuilder: (_, i) {
                                        final caty = categories[i];
                                        return CardChannelItemNew(
                                          catySelected: selectedCaty == caty,
                                          name: caty.categoryName ?? "",
                                          catyId: selectedCaty == caty
                                              ? selectedCaty!.categoryId
                                              : null,
                                          futureChannels: featureChannels,
                                          onTap: () {
                                            if (selectedCaty == caty) {
                                              debugPrint("same caty");
                                              return false;
                                            }
                                            setState(() {
                                              selectedCaty = caty;
                                              featureChannels = IpTvApi()
                                                  .getLiveChannels(
                                                      caty.categoryId!);
                                            });
                                          },
                                          selectedLive: channelLive,
                                          onChannel:
                                              (ChannelLive channel) async {
                                            //TODO: channel selected
                                            final link =
                                                "${userAuth.serverInfo!.serverUrl}/${userAuth.userInfo!.username}/${userAuth.userInfo!.password}/${channel.streamId}";

                                            try {
                                              debugPrint("link: $link");

                                              if (_videoPlayerController !=
                                                      null &&
                                                  (await _videoPlayerController!
                                                          .isPlaying() ??
                                                      false)) {
                                                if (mounted) {
                                                  _videoPlayerController!
                                                      .pause();
                                                  _videoPlayerController = null;
                                                  setState(() {});
                                                }
                                              }

                                              await Future.delayed(
                                                      const Duration(
                                                          milliseconds: 100))
                                                  .then((value) {
                                                ///Play new Stream
                                                debugPrint("Play new Stream");

                                                selectedVideo = i;
                                                _videoPlayerController =
                                                    VlcPlayerController.network(
                                                  link,
                                                  hwAcc: HwAcc.full,
                                                  autoPlay: true,
                                                  autoInitialize: true,
                                                  options: VlcPlayerOptions(),
                                                );
                                                if (mounted) {
                                                  setState(() {
                                                    channelLive = channel;
                                                    selectedStreamIdEpg =
                                                        channel.streamId;
                                                  });
                                                }
                                              });
                                            } catch (e) {
                                              debugPrint("error: $e");
                                              _videoPlayerController = null;
                                              setState(() {
                                                channelLive = channel;
                                                selectedStreamIdEpg =
                                                    channel.streamId;
                                              });
                                            }
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                      ],
                    );
                  }

                  return const SizedBox();
                },
              );
            }

            return const SizedBox();
          },
        ),
      ),
    );
  }
}

/*
return BlocBuilder<ChannelsBloc, ChannelsState>(
              builder: (context, state) {
                if (state is ChannelsLoading) {
                  return const Center(child: CircularProgressIndicator());
                } else if (state is ChannelsLiveSuccess) {
                  final channels = state.channels;

                  return Stack(
                    children: [
                      if (selectedVideo != null &&
                          _videoPlayerController != null)
                        SizedBox(
                          width: 100.w,
                          height: 100.h,
                          child: VlcPlayer(
                            controller: _videoPlayerController!,
                            aspectRatio: 16 / 9,
                            placeholder: const Center(
                                child: CircularProgressIndicator()),
                          ),
                        ),

                      ///SHow Drawer or Hide
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            showDrawer = !showDrawer;
                          });
                        },
                        child: Container(
                          width: 100.w,
                          height: 100.w,
                          color: Colors.transparent,
                        ),
                      ),
                      if (showDrawer)
                        SizedBox(
                          width: 35.w,
                          height: 100.h,
                          child: Material(
                            color: Colors.black45,
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                  const EdgeInsets.symmetric(vertical: 5),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      IconButton(
                                        onPressed: () => Get.back(),
                                        focusColor: kColorPrimaryDark,
                                        icon: const Icon(Icons.arrow_back),
                                      ),
                                      Text(
                                        'Live',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15.sp,
                                        ),
                                      ),
                                      const SizedBox(),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    itemBuilder: (_, i) {
                                      final model = channels[i];
                                      return CardChannelItemNew(
                                        image: model.streamIcon ?? "",
                                        name: model.name ?? "",
                                        isSelected: selectedVideo == i,
                                        onTap: () async {
                                          final link =
                                              "${userAuth.serverInfo!.serverUrl}/${userAuth.userInfo!.username}/${userAuth.userInfo!.password}/${model.streamId}";

                                          try {
                                            debugPrint("link: $link");
                                            if (selectedVideo == i &&
                                                _videoPlayerController !=
                                                    null) {
                                              // OPEN FULL SCREEN
                                              debugPrint(
                                                  "///////////// OPEN FULL STREAM /////////////");
                                              context
                                                  .read<VideoCubit>()
                                                  .changeUrlVideo(true);
                                            } else {
                                              if (_videoPlayerController !=
                                                  null &&
                                                  (await _videoPlayerController!
                                                      .isPlaying() ??
                                                      false)) {
                                                if (mounted) {
                                                  _videoPlayerController!
                                                      .pause();
                                                  _videoPlayerController =
                                                  null;
                                                  setState(() {});
                                                }
                                              }

                                              await Future.delayed(
                                                  const Duration(
                                                      milliseconds: 100))
                                                  .then((value) {
                                                ///Play new Stream
                                                debugPrint("Play new Stream");

                                                selectedVideo = i;
                                                _videoPlayerController =
                                                    VlcPlayerController
                                                        .network(
                                                      link,
                                                      hwAcc: HwAcc.full,
                                                      autoPlay: true,
                                                      autoInitialize: true,
                                                      options: VlcPlayerOptions(),
                                                    );
                                                if (mounted) {
                                                  setState(() {
                                                    channelLive = model;
                                                    selectedStreamIdEpg =
                                                        model.streamId;
                                                  });
                                                }
                                              });
                                            }
                                          } catch (e) {
                                            debugPrint("error: $e");
                                            _videoPlayerController = null;
                                            setState(() {
                                              channelLive = model;
                                              selectedStreamIdEpg =
                                                  model.streamId;
                                            });
                                          }
                                        },
                                      );
                                    },
                                    itemCount: channels.length,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  );
                }
                return const SizedBox();
              },
            );
 */

/*class LiveChannelsScreen extends StatefulWidget {
  const LiveChannelsScreen({Key? key, required this.catyId}) : super(key: key);
  final String catyId;

  @override
  State<LiveChannelsScreen> createState() => _ListChannelsScreen();
}

class _ListChannelsScreen extends State<LiveChannelsScreen> {
  VlcPlayerController? _videoPlayerController;
  int? selectedVideo;
  String? selectedStreamId;
  ChannelLive? channelLive;
  double lastPosition = 0.0;
  String keySearch = "";

  @override
  void initState() {
    context.read<ChannelsBloc>().add(GetLiveChannelsEvent(
          catyId: widget.catyId,
          typeCategory: TypeCategory.live,
        ));
    super.initState();
  }

  @override
  void dispose() async {
    super.dispose();
    if (_videoPlayerController != null) {
      await _videoPlayerController!.stopRendererScanning();
      await _videoPlayerController!.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, stateAuth) {
          if (stateAuth is AuthSuccess) {
            final userAuth = stateAuth.user;

            return Scaffold(
              body: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Ink(
                    width: 100.w,
                    height: 100.h,
                    decoration: kDecorBackground,
                    // padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10),
                    child: Column(
                      children: [
                        BlocBuilder<VideoCubit, VideoState>(
                          builder: (context, stateVideo) {
                            if (stateVideo.isFull) {
                              return const SizedBox();
                            }
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 3.h),
                                BlocBuilder<FavoritesCubit, FavoritesState>(
                                  builder: (context, state) {
                                    final isLiked = channelLive == null
                                        ? false
                                        : state.lives
                                            .where((live) =>
                                                live.streamId ==
                                                channelLive!.streamId)
                                            .isNotEmpty;
                                    return AppBarLive(
                                      isLiked: isLiked,
                                      onLike: channelLive == null
                                          ? null
                                          : () {
                                              context
                                                  .read<FavoritesCubit>()
                                                  .addLive(channelLive,
                                                      isAdd: !isLiked);
                                            },
                                      onSearch: (String value) {
                                        setState(() {
                                          keySearch = value;
                                        });
                                      },
                                    );
                                  },
                                ),
                                const SizedBox(height: 15),
                              ],
                            );
                          },
                        ),
                        Expanded(
                          child: Row(
                            children: [
                              BlocBuilder<VideoCubit, VideoState>(
                                builder: (context, stateVideo) {
                                  bool setFull = stateVideo.isFull;
                                  if (setFull) {
                                    return const SizedBox();
                                  }
                                  return Expanded(
                                    child: BlocBuilder<ChannelsBloc,
                                        ChannelsState>(
                                      builder: (context, state) {
                                        if (state is ChannelsLoading) {
                                          return const Center(
                                              child:
                                                  CircularProgressIndicator());
                                        } else if (state
                                            is ChannelsLiveSuccess) {
                                          final categories = state.channels;

                                          List<ChannelLive> searchList =
                                              categories
                                                  .where((element) => element
                                                      .name!
                                                      .toLowerCase()
                                                      .contains(keySearch))
                                                  .toList();

                                          return GridView.builder(
                                            padding: const EdgeInsets.only(
                                              left: 10,
                                              right: 10,
                                              bottom: 80,
                                            ),
                                            itemCount: keySearch.isEmpty
                                                ? categories.length
                                                : searchList.length,
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount:
                                                  selectedVideo == null ? 2 : 1,
                                              mainAxisSpacing: 10,
                                              crossAxisSpacing:
                                                  selectedVideo == null
                                                      ? 10
                                                      : 0,
                                              childAspectRatio: 7,
                                            ),
                                            itemBuilder: (_, i) {
                                              final model = keySearch.isEmpty
                                                  ? categories[i]
                                                  : searchList[i];

                                              final link =
                                                  "${userAuth.serverInfo!.serverUrl}/${userAuth.userInfo!.username}/${userAuth.userInfo!.password}/${model.streamId}";

                                              return CardLiveItem(
                                                title: model.name ?? "",
                                                image: model.streamIcon,
                                                link: link,
                                                isSelected:
                                                    selectedVideo == null
                                                        ? false
                                                        : selectedVideo == i,
                                                onTap: () async {
                                                  try {
                                                    debugPrint("link: $link");
                                                    if (selectedVideo == i &&
                                                        _videoPlayerController !=
                                                            null) {
                                                      // OPEN FULL SCREEN
                                                      debugPrint(
                                                          "///////////// OPEN FULL STREAM /////////////");
                                                      context
                                                          .read<VideoCubit>()
                                                          .changeUrlVideo(true);
                                                    } else {
                                                      if (_videoPlayerController !=
                                                              null &&
                                                          (await _videoPlayerController!
                                                                  .isPlaying() ??
                                                              false)) {
                                                        if (mounted) {
                                                          _videoPlayerController!
                                                              .pause();
                                                          _videoPlayerController =
                                                              null;
                                                          setState(() {});
                                                        }
                                                      }

                                                      await Future.delayed(
                                                              const Duration(
                                                                  milliseconds:
                                                                      100))
                                                          .then((value) {
                                                        ///Play new Stream
                                                        debugPrint(
                                                            "Play new Stream");

                                                        selectedVideo = i;
                                                        _videoPlayerController =
                                                            VlcPlayerController
                                                                .network(
                                                          link,
                                                          hwAcc: HwAcc.full,
                                                          autoPlay: true,
                                                          autoInitialize: true,
                                                          options:
                                                              VlcPlayerOptions(),
                                                        );
                                                        if (mounted) {
                                                          setState(() {
                                                            channelLive = model;
                                                            selectedStreamId =
                                                                model.streamId;
                                                          });
                                                        }
                                                      });
                                                    }
                                                  } catch (e) {
                                                    debugPrint("error: $e");
                                                    //  context.read<VideoCubit>().changeUrlVideo(false);

                                                    // selectedVideo = null;
                                                    _videoPlayerController =
                                                        null;
                                                    setState(() {
                                                      channelLive = model;
                                                      selectedStreamId =
                                                          model.streamId;
                                                    });
                                                  }
                                                },
                                              );
                                            },
                                          );
                                        }

                                        return const Center(
                                          child: Text("Failed to load data..."),
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                              if (selectedVideo != null)
                                Expanded(
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: StreamPlayerPage(
                                          controller: _videoPlayerController,
                                        ),
                                      ),
                                      BlocBuilder<VideoCubit, VideoState>(
                                        builder: (context, stateVideo) {
                                          if (stateVideo.isFull) {
                                            return const SizedBox();
                                          }

                                          ///Get EPG
                                          return CardEpgStream(
                                              streamId: selectedStreamId);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (selectedVideo == null) AdmobWidget.getBanner(),
                ],
              ),
            );
          }

          return const Scaffold();
        },
      ),
    );
  }
}*/
