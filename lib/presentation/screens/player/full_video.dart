part of '../screens.dart';

class FullVideoScreen extends StatefulWidget {
  const FullVideoScreen({
    Key? key,
    required this.link,
    required this.title,
    this.isLive = false,
  }) : super(key: key);
  final String link;
  final String title;
  final bool isLive;

  @override
  State<FullVideoScreen> createState() => _FullVideoScreenState();
}

class _FullVideoScreenState extends State<FullVideoScreen> {
  late VlcPlayerController _videoPlayerController;
  bool isPlayed = true;
  bool progress = true;
  bool showControllersVideo = true;
  String position = '';
  String duration = '';
  double sliderValue = 0.0;
  bool validPosition = false;
  double _currentVolume = 0.0;
  double _currentBright = 0.0;
  late Timer timer;

  _settingPage() async {
    try {
      ///default volume is half
      _currentBright = await ScreenBrightness().current;
      _currentVolume = await PerfectVolumeControl.volume;

      setState(() {});
    } catch (e) {
      debugPrint("Error: setting: $e");
    }
  }

  final FocusNode _remoteFocus = FocusNode();
  final FocusNode _remotePlayPause = FocusNode();
  final FocusNode _backFocus = FocusNode();
  final FocusNode _aboveFocus = FocusNode();
  final FocusNode _nextFocus = FocusNode();
  final FocusNode sliderFocus = FocusNode();
  int indexButtons = 0;

  _keyCode(RawKeyEvent event) {
    debugPrint("EVENT");

    if (event.isKeyPressed(LogicalKeyboardKey.arrowDown)) {
      debugPrint('downd');

      if (indexButtons == 0) {
        indexButtons = 1;
        _remotePlayPause.requestFocus();
      }
    } else if (event.isKeyPressed(LogicalKeyboardKey.arrowUp)) {
      debugPrint('up');

      indexButtons = 0;
      sliderFocus.unfocus();
      _backFocus.requestFocus();
    } else if (event.isKeyPressed(LogicalKeyboardKey.arrowRight)) {
      debugPrint('right');
      if (indexButtons == 1) {
        indexButtons = 2;
        _aboveFocus.requestFocus();
      } else if (indexButtons == 2) {
        indexButtons = 3;
        _nextFocus.requestFocus();
      }
    } else if (event.isKeyPressed(LogicalKeyboardKey.arrowLeft)) {
      debugPrint('left');
      if (indexButtons == 3) {
        indexButtons = 2;
        _aboveFocus.requestFocus();
      } else if (indexButtons == 2) {
        indexButtons = 1;
        _remotePlayPause.requestFocus();
      }
    } else if (event.isKeyPressed(LogicalKeyboardKey.enter) ||
        event.isKeyPressed(LogicalKeyboardKey.select)) {
      debugPrint("enter");

      //Back
      if (indexButtons == 0) {
        Get.back();
      } else if (indexButtons == 1) {
        _onPayPause();
      } else if (indexButtons == 2) {
        _onAbove(true);
      } else if (indexButtons == 3) {
        _onAbove(false);
      }
    }

    // _remoteFocus.requestFocus();
    setState(() {
      showControllersVideo = true;
    });
  }

  @override
  void initState() {
    Wakelock.enable();
    _videoPlayerController = VlcPlayerController.network(
      widget.link,
      hwAcc: HwAcc.full,
      autoPlay: true,
      autoInitialize: true,
      options: VlcPlayerOptions(),
    );

    super.initState();
    _videoPlayerController.addListener(listener);
    _settingPage();

    //  debugPrint("initial controller");

    timer = Timer.periodic(const Duration(seconds: 6), (timer) {
      if (showControllersVideo) {
        debugPrint("Hide Controllers");
        setState(() {
          showControllersVideo = false;
        });
      }
    });
  }

  void listener() async {
    if (!mounted) return;

    if (progress) {
      if (_videoPlayerController.value.isPlaying) {
        setState(() {
          progress = false;
        });
      }
    }

    if (_videoPlayerController.value.isInitialized) {
      var oPosition = _videoPlayerController.value.position;
      var oDuration = _videoPlayerController.value.duration;

      if (oDuration.inHours == 0) {
        var strPosition = oPosition.toString().split('.')[0];
        var strDuration = oDuration.toString().split('.')[0];
        position = "${strPosition.split(':')[1]}:${strPosition.split(':')[2]}";
        duration = "${strDuration.split(':')[1]}:${strDuration.split(':')[2]}";
      } else {
        position = oPosition.toString().split('.')[0];
        duration = oDuration.toString().split('.')[0];
      }
      validPosition = oDuration.compareTo(oPosition) >= 0;
      sliderValue = validPosition ? oPosition.inSeconds.toDouble() : 0;
      setState(() {});
    }
  }

  void _onSliderPositionChanged(double progress) {
    setState(() {
      sliderValue = progress.floor().toDouble();
    });
    //convert to Milliseconds since VLC requires MS to set time
    _videoPlayerController.setTime(sliderValue.toInt() * 1000);
  }

  _onPayPause() {
    setState(() {
      if (isPlayed) {
        _videoPlayerController.pause();
        isPlayed = false;
      } else {
        _videoPlayerController.play();
        isPlayed = true;
      }
    });
  }

  _onAbove(bool isAbove) {
    Duration currentPosition = _videoPlayerController.value.position;

    Duration newPosition = isAbove
        ? currentPosition - const Duration(seconds: 30)
        : currentPosition + const Duration(seconds: 30);

    _videoPlayerController.seekTo(newPosition);
    setState(() {});
  }

  @override
  void dispose() async {
    Wakelock.disable();
    super.dispose();
    await _videoPlayerController.stopRendererScanning();
    await _videoPlayerController.dispose();
    timer.cancel();
    _remoteFocus.dispose();
    _backFocus.dispose();
    _aboveFocus.dispose();
    _nextFocus.dispose();
    sliderFocus.dispose();
    _remotePlayPause.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: RawKeyboardListener(
        focusNode: _remoteFocus,
        onKey: _keyCode,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Ink(
              width: getSize(context).width,
              height: getSize(context).height,
              color: Colors.black,
              child: VlcPlayer(
                controller: _videoPlayerController,
                aspectRatio: 16 / 9,
                virtualDisplay: true,
                placeholder: const SizedBox(),
              ),
            ),

            if (progress)
              const Center(
                  child: CircularProgressIndicator(
                color: kColorPrimary,
              )),

            ///Controllers
            GestureDetector(
              onTap: () {
                setState(() {
                  showControllersVideo = !showControllersVideo;
                });
              },
              child: Container(
                width: getSize(context).width,
                height: getSize(context).height,
                color: Colors.transparent,
                child: AnimatedSize(
                  duration: const Duration(milliseconds: 200),
                  child: !showControllersVideo
                      ? const SizedBox()
                      : SafeArea(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ///Back & Title
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CardPlayerItem(
                                    focusNode: _backFocus,
                                    autofocus: true,
                                    isSelected: indexButtons == 0,
                                    onPressed: () async {
                                      await Future.delayed(const Duration(
                                              milliseconds: 1000))
                                          .then((value) {
                                        Get.back(
                                            result: progress
                                                ? null
                                                : [
                                                    sliderValue,
                                                    _videoPlayerController.value
                                                        .duration.inSeconds
                                                        .toDouble()
                                                  ]);
                                      });
                                    },
                                    icon: Icon(
                                      FontAwesomeIcons.chevronLeft,
                                      size: 19.sp,
                                    ),
                                  ),
                                  const SizedBox(width: 5),
                                  Expanded(
                                    child: Text(
                                      widget.title,
                                      maxLines: 1,
                                      style: Get.textTheme.labelLarge!.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.sp,
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              ///Slider & Play/Pause
                              //  if (!progress && !widget.isLive)
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Row(
                                  children: [
                                    CardPlayerItem(
                                      focusNode: _remotePlayPause,
                                      isSelected: indexButtons == 1,
                                      onPressed: () {
                                        _onPayPause();
                                      },
                                      icon: Icon(
                                        isPlayed
                                            ? FontAwesomeIcons.pause
                                            : FontAwesomeIcons.play,
                                        size: 28,
                                      ),
                                    ),
                                    CardPlayerItem(
                                      focusNode: _aboveFocus,
                                      isSelected: indexButtons == 2,
                                      onPressed: () {
                                        _onAbove(true);
                                      },
                                      icon: const Icon(
                                        FontAwesomeIcons.rotateLeft,
                                        size: 18,
                                      ),
                                    ),
                                    CardPlayerItem(
                                      focusNode: _nextFocus,
                                      isSelected: indexButtons == 3,
                                      onPressed: () {
                                        _onAbove(false);
                                      },
                                      icon: const Icon(
                                        FontAwesomeIcons.rotateRight,
                                        size: 18,
                                      ),
                                    ),
                                    Expanded(
                                      child: Slider(
                                        focusNode: sliderFocus,
                                        activeColor: kColorPrimary,
                                        autofocus: false,
                                        inactiveColor: Colors.white,
                                        value: sliderValue,
                                        min: 0.0,
                                        max: (!validPosition)
                                            ? 1.0
                                            : _videoPlayerController
                                                .value.duration.inSeconds
                                                .toDouble(),
                                        onChanged: validPosition
                                            ? _onSliderPositionChanged
                                            : null,
                                      ),
                                    ),
                                    Text(
                                      "$position / $duration",
                                      style: Get.textTheme.titleSmall!.copyWith(
                                        fontSize: 15.sp,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                ),
              ),
            ),

            if (!progress && showControllersVideo)

              ///Controllers Light, Lock...
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (!isTv(context))
                    FillingSlider(
                      direction: FillingSliderDirection.vertical,
                      initialValue: _currentVolume,
                      onFinish: (value) async {
                        PerfectVolumeControl.hideUI = true;
                        await PerfectVolumeControl.setVolume(value);
                        setState(() {
                          _currentVolume = value;
                        });
                      },
                      fillColor: Colors.white54,
                      height: 40.h,
                      width: 30,
                      child: Icon(
                        _currentVolume < .1
                            ? FontAwesomeIcons.volumeXmark
                            : _currentVolume < .7
                                ? FontAwesomeIcons.volumeLow
                                : FontAwesomeIcons.volumeHigh,
                        color: Colors.black,
                        size: 13,
                      ),
                    ),
                  if (!isTv(context))
                    FillingSlider(
                      initialValue: _currentBright,
                      direction: FillingSliderDirection.vertical,
                      fillColor: Colors.white54,
                      height: 40.h,
                      width: 30,
                      onFinish: (value) async {
                        await ScreenBrightness().setScreenBrightness(value);
                        setState(() {
                          _currentBright = value;
                        });
                      },
                      child: Icon(
                        _currentBright < .1
                            ? FontAwesomeIcons.moon
                            : _currentVolume < .7
                                ? FontAwesomeIcons.sun
                                : FontAwesomeIcons.solidSun,
                        color: Colors.black,
                        size: 13,
                      ),
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}

class CardPlayerItem extends StatelessWidget {
  const CardPlayerItem(
      {super.key,
      required this.focusNode,
      this.autofocus = false,
      required this.isSelected,
      required this.onPressed,
      required this.icon});
  final FocusNode focusNode;
  final Function() onPressed;
  final Icon icon;
  final bool autofocus;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
          focusNode: focusNode,
          focusColor: Colors.transparent,
          onPressed: onPressed,
          icon: Icon(
            icon.icon,
            color: isSelected ? kColorPrimary : Colors.white,
          ),
        ),
      ],
    );
  }
}
