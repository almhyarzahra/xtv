part of '../screens.dart';

class StreamPlayerPage extends StatelessWidget {
  const StreamPlayerPage({Key? key, required this.controller})
      : super(key: key);
  final VlcPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Ink(
      color: Colors.black,
      width: 100.w,
      height: 100.h,
      child: VlcPlayer(
        controller: controller,
        aspectRatio: 16 / 9,
        placeholder: const Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
