part of 'screens.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  _loadIntel() async {
    if (!showAds) {
      return false;
    }
  }

  final FocusNode liveFocus = FocusNode();
  final FocusNode movieFocus = FocusNode();
  final FocusNode serieFocus = FocusNode();

  @override
  void initState() {
    context.read<FavoritesCubit>().initialData();
    context.read<WatchingCubit>().initialData();
    _loadIntel();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      //FocusScope.of(context).requestFocus(liveFocus);
      liveFocus.requestFocus();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Ink(
        width: getSize(context).width,
        height: getSize(context).height,
        decoration: kDecorBackground,
        padding: EdgeInsets.only(
            left: 10, right: 10, top: getSize(context).height * .03),
        child: Column(
          children: [
            const AppBarWelcome(),
            //const SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CardWelcomeSetting(
                  title: 'Catch up',
                  icon: Icons.refresh_rounded,
                  onTap: () {
                    Get.toNamed(screenCatchUp);
                  },
                ),
                CardWelcomeSetting(
                  title: 'Favourites',
                  icon: Icons.favorite,
                  onTap: () {
                    Get.toNamed(screenFavourite);
                  },
                ),
                CardWelcomeSetting(
                  title: 'Settings',
                  icon: FontAwesomeIcons.gear,
                  onTap: () {
                    Get.toNamed(screenSettings);
                  },
                ),
              ],
            ),

            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    Expanded(
                      child: BlocBuilder<LiveCatyBloc, LiveCatyState>(
                        builder: (context, state) {
                          if (state is LiveCatyLoading) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (state is LiveCatySuccess) {
                            return CardWelcomeTv(
                              title: "LIVE TV",
                              subTitle: "${state.categories.length} Channels",
                              icon: kIconLive,
                              autofocus: true,
                              focusNode: liveFocus,
                              onTap: () {
                                debugPrint("CLick");
                                Get.to(() => const LiveChannelsScreen())!
                                    .then((value) async {
                                  liveFocus.requestFocus();
                                });
                              },
                            );
                          }

                          return const Text('error live caty');
                        },
                      ),
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: BlocBuilder<MovieCatyBloc, MovieCatyState>(
                        builder: (context, state) {
                          if (state is MovieCatyLoading) {
                            return const Center(
                                child: CircularProgressIndicator());
                          } else if (state is MovieCatySuccess) {
                            return CardWelcomeTv(
                              title: "Movies",
                              subTitle: "${state.categories.length} Channels",
                              icon: kIconMovies,
                              focusNode: movieFocus,
                              onTap: () {
                                Get.toNamed(screenMovieCategories)!
                                    .then((value) async {
                                  movieFocus.requestFocus();
                                });
                              },
                            );
                          }

                          return const Text('error movie caty');
                        },
                      ),
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: BlocBuilder<SeriesCatyBloc, SeriesCatyState>(
                        builder: (context, state) {
                          if (state is SeriesCatyLoading) {
                            return const Center(
                                child: CircularProgressIndicator());
                          } else if (state is SeriesCatySuccess) {
                            return CardWelcomeTv(
                              title: "Series",
                              subTitle: "${state.categories.length} Channels",
                              icon: kIconSeries,
                              focusNode: serieFocus,
                              onTap: () {
                                Get.toNamed(screenSeriesCategories)!
                                    .then((value) async {
                                  serieFocus.requestFocus();
                                });
                              },
                            );
                          }

                          return const Text('could not load series');
                        },
                      ),
                    ),
                    // const SizedBox(width: 15),
                    // SizedBox(
                    //   width: getSize(context).width * .20,
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //     children: [
                    //       CardWelcomeSetting(
                    //         title: 'Catch up',
                    //         icon: Icons.refresh_rounded,
                    //         onTap: () {
                    //           Get.toNamed(screenCatchUp);
                    //         },
                    //       ),
                    //       CardWelcomeSetting(
                    //         title: 'Favourites',
                    //         icon: Icons.favorite,
                    //         onTap: () {
                    //           Get.toNamed(screenFavourite);
                    //         },
                    //       ),
                    //       CardWelcomeSetting(
                    //         title: 'Settings',
                    //         icon: FontAwesomeIcons.gear,
                    //         onTap: () {
                    //           Get.toNamed(screenSettings);
                    //         },
                    //       ),
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
            /*   Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'By using this application, you agree to the',
                  style: Get.textTheme.subtitle2!.copyWith(
                    fontSize: 12.sp,
                    color: Colors.grey,
                  ),
                ),
                InkWell(
                  onTap: () async {
                    await launchUrlString(kPrivacy);
                  },
                  child: Text(
                    ' Terms of Services.',
                    style: Get.textTheme.subtitle2!.copyWith(
                      fontSize: 12.sp,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ],
            ),*/
            AdmobWidget.getBanner(),
          ],
        ),
      ),
    );
  }
}
