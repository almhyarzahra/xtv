part of 'widgets.dart';

class AppBarWelcome extends StatelessWidget {
  const AppBarWelcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getSize(context).width,
      height: getSize(context).height * .11,
      // margin: EdgeInsets.symmetric(vertical: 7.h, horizontal: 15),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(screenWidth(40)),
            child: Image(
              width: 0.3.dp,
              height: 0.3.dp,
              image: const AssetImage(kIconSplash),
            ),
          ),
          const SizedBox(width: 5),
          Text(
            kAppName,
            style: Get.textTheme.headline4,
          ),
          Container(
            width: 1,
            height: 8.h,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            color: kColorHint,
          ),
          Expanded(
            child: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                if (state is AuthSuccess) {
                  final userInfo = state.user.userInfo;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        dateNowWelcome(),
                        style: Get.textTheme.subtitle2,
                      ),
                      if (expirationDate(userInfo!.expDate) != null)
                        Text(
                          "Expiration: ${expirationDate(userInfo!.expDate)}",
                          style: Get.textTheme.subtitle2!.copyWith(
                            color: kColorHint,
                          ),
                        ),
                    ],
                  );
                }

                return const SizedBox();
              },
            ),
          ),
          // IconButton(
          //   focusColor: kColorFocus,
          //   onPressed: () {
          //     Get.toNamed(screenSettings);
          //   },
          //   icon: Icon(
          //     FontAwesomeIcons.gear,
          //     color: Colors.white,
          //     size: 19.sp,
          //   ),
          // ),
        ],
      ),
    );
  }
}

class CardWelcomeSetting extends StatelessWidget {
  const CardWelcomeSetting(
      {Key? key, required this.title, required this.icon, required this.onTap})
      : super(key: key);
  final String title;
  final IconData icon;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(20),
      focusColor: kColorFocus,
      child: Row(
        children: [
          Ink(
            width: 7.w,
            height: 7.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: const RadialGradient(colors: [
                kColorPrimary,
                kColorCardLight,
              ]),
            ),
            child: Center(
              child: Icon(icon),
            ),
          ),
          const SizedBox(width: 10),
          Text(
            title,
            style: Get.textTheme.headline5,
          ),
        ],
      ),
    );
  }
}

class CardWelcomeTv extends StatelessWidget {
  const CardWelcomeTv({
    Key? key,
    required this.icon,
    required this.onTap,
    required this.title,
    required this.subTitle,
    this.autofocus = false,
    this.focusNode,
  }) : super(key: key);
  final String icon;
  final String title;
  final String subTitle;
  final bool autofocus;
  final Function() onTap;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      focusNode: focusNode,
      autofocus: autofocus,
      borderRadius: BorderRadius.circular(30),
      focusColor: kColorFocus,
      child: Ink(
        decoration: BoxDecoration(
          color: kColorPrimary.withOpacity(0.6),
          borderRadius: BorderRadius.circular(3),
        ),
        padding: const EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
              width: getSize(context).width * .08,
              image: AssetImage(icon),
            ),
            SizedBox(height: getSize(context).height * .03),
            Text(
              title,
              style: Get.textTheme.headline3,
            ),
            /* SizedBox(height: 1.h),
            Text(
              "◍ $subTitle",
              style: Get.textTheme.subtitle2!.copyWith(color: Colors.white70),
            ),*/
          ],
        ),
      ),
    );
  }
}

class CardTallButton extends StatelessWidget {
  const CardTallButton(
      {Key? key,
      required this.label,
      required this.onTap,
      this.radius = 5,
      this.isLoading = false})
      : super(key: key);
  final String label;
  final Function() onTap;
  final double radius;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedContainer(
        width: 100.w,
        height: 55,
        duration: const Duration(milliseconds: 300),
        child: ElevatedButton(
          onPressed: onTap,
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(kColorPrimary),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius),
              ))),
          child: isLoading
              ? Container(
                  width: screenWidth(5),
                  height: screenWidth(5),
                  decoration: BoxDecoration(
                      color: AppColors.mainBlueColor.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(15)),
                  child: Lottie.asset("assets/images/loading.json"),
                )
              : Text(
                  label,
                  style: Get.textTheme.headlineLarge!.copyWith(
                    color: Colors.white,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
        ),
      ),
    );
  }
}

class LoadingWidgt extends StatelessWidget {
  const LoadingWidgt({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getSize(context).width,
      height: getSize(context).height,
      decoration: kDecorBackground,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Hero(
            tag: "splash",
            child: Image(
              width: 0.5.dp,
              height: 0.5.dp,
              image: const AssetImage(kIconSplash),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            kAppName,
            style: Get.textTheme.headline3,
          ),
          BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state) {
              if (state is AuthLoading) {
                return Container(
                  margin: const EdgeInsets.symmetric(vertical: 15),
                  child: const CircularProgressIndicator(),
                );
              } else if (state is AuthFailed) {
                return const Text('');
              }
              return const SizedBox();
            },
          ),
        ],
      ),
    );
  }
}
