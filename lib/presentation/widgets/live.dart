part of 'widgets.dart';

class AppBarLive extends StatefulWidget {
  const AppBarLive({
    Key? key,
    this.onSearch,
    this.isLiked = false,
    this.onLike,
  }) : super(key: key);
  final Function(String)? onSearch;
  final bool isLiked;
  final Function()? onLike;

  @override
  State<AppBarLive> createState() => _AppBarLiveState();
}

class _AppBarLiveState extends State<AppBarLive> {
  bool showSearch = false;
  final _search = TextEditingController();

  @override
  void dispose() {
    _search.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      color: Colors.transparent,
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Image(
            width: 0.3.dp,
            height: 0.3.dp,
            image: const AssetImage(kIconSplash),
          ),
          const SizedBox(width: 5),
          Text(
            kAppName,
            style: Get.textTheme.headline4,
          ),
          Container(
            width: 1,
            height: 8.h,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            color: kColorHint,
          ),
          Image(height: 9.h, image: const AssetImage(kIconLive)),
          const Spacer(),
          showSearch
              ? Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 15),
                    child: TextField(
                      controller: _search,
                      onChanged: widget.onSearch,
                      decoration: const InputDecoration(
                        hintText: "Search...",
                      ),
                    ),
                  ),
                )
              : const Spacer(),
          if (showSearch)
            IconButton(
              icon: const Icon(
                FontAwesomeIcons.xmark,
                color: Colors.white,
              ),
              focusColor: kColorPrimary,
              onPressed: () {
                if (widget.onSearch != null) {
                  widget.onSearch!("");
                }

                setState(() {
                  showSearch = false;
                  _search.clear();
                });
              },
            ),
          if (!showSearch)
            IconButton(
              focusColor: kColorFocus,
              onPressed: () {
                setState(() {
                  showSearch = true;
                });
              },
              icon: const Icon(
                FontAwesomeIcons.magnifyingGlass,
                color: Colors.white,
              ),
            ),
          if (widget.onLike != null)
            IconButton(
              focusColor: kColorFocus,
              onPressed: widget.onLike,
              icon: Icon(
                widget.isLiked
                    ? FontAwesomeIcons.solidHeart
                    : FontAwesomeIcons.heart,
                color: Colors.white,
              ),
            ),
          IconButton(
            focusColor: kColorFocus,
            onPressed: () {
              context.read<VideoCubit>().changeUrlVideo(false);
              Get.back();
            },
            icon: const Icon(
              FontAwesomeIcons.chevronRight,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

class AppBarMovie extends StatefulWidget {
  const AppBarMovie(
      {Key? key,
      this.onSearch,
      this.onFavorite,
      this.top,
      this.isLiked = false})
      : super(key: key);

  final Function()? onFavorite;
  final Function(String)? onSearch;
  final double? top;
  final bool isLiked;

  @override
  State<AppBarMovie> createState() => _AppBarMovieState();
}

class _AppBarMovieState extends State<AppBarMovie> {
  bool showSearch = false;
  final _search = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      color: Colors.transparent,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: widget.top ?? 2.h),
      child: Material(
        color: Colors.transparent,
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(60)),
              child: const Image(
                width: 40,
                height: 40,
                image: AssetImage(kIconSplash),
              ),
            ),
            const SizedBox(width: 5),
            Text(
              kAppName,
              style: Get.textTheme.headline4,
            ),
            Container(
              width: 1,
              height: 30,
              margin: const EdgeInsets.symmetric(horizontal: 13),
              color: kColorHint,
            ),
            const Image(height: 30, image: AssetImage(kIconMovies)),
            const Spacer(),
            showSearch
                ? Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: TextField(
                        controller: _search,
                        onChanged: widget.onSearch,
                        decoration: const InputDecoration(
                          hintText: "Search...",
                        ),
                      ),
                    ),
                  )
                : const Spacer(),
            if (showSearch)
              IconButton(
                icon: const Icon(
                  FontAwesomeIcons.xmark,
                  color: Colors.white,
                ),
                focusColor: kColorPrimary,
                onPressed: () {
                  if (widget.onSearch != null) {
                    widget.onSearch!("");
                  }

                  setState(() {
                    showSearch = false;
                    _search.clear();
                  });
                },
              ),
            if (widget.onSearch != null)
              if (!showSearch)
                IconButton(
                  focusColor: kColorFocus,
                  onPressed: () {
                    setState(() {
                      showSearch = true;
                    });
                  },
                  icon: const Icon(
                    FontAwesomeIcons.magnifyingGlass,
                    color: Colors.white,
                  ),
                ),
            if (widget.onFavorite != null)
              IconButton(
                focusColor: kColorFocus,
                onPressed: widget.onFavorite,
                icon: Icon(
                  widget.isLiked
                      ? FontAwesomeIcons.solidHeart
                      : FontAwesomeIcons.heart,
                  color: Colors.white,
                ),
              ),
            IconButton(
              focusColor: kColorFocus,
              onPressed: () {
                context.read<VideoCubit>().changeUrlVideo(false);
                Get.back();
              },
              icon: const Icon(
                FontAwesomeIcons.chevronRight,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AppBarSeries extends StatefulWidget {
  const AppBarSeries(
      {Key? key,
      this.onSearch,
      this.onFavorite,
      this.top,
      this.isLiked = false})
      : super(key: key);

  final Function()? onFavorite;
  final Function(String)? onSearch;
  final double? top;
  final bool isLiked;

  @override
  State<AppBarSeries> createState() => _AppBarSeriesState();
}

class _AppBarSeriesState extends State<AppBarSeries> {
  bool showSearch = false;
  final _search = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      margin: EdgeInsets.only(top: widget.top ?? 15, left: 10, right: 10),
      child: Material(
        color: Colors.transparent,
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(screenWidth(60)),
              child: const Image(
                width: 40,
                height: 40,
                image: AssetImage(kIconSplash),
              ),
            ),
            const SizedBox(width: 5),
            Text(
              kAppName,
              style: Get.textTheme.headline4,
            ),
            Container(
              width: 1,
              height: 8.h,
              margin: const EdgeInsets.symmetric(horizontal: 13),
              color: kColorHint,
            ),
            const Image(height: 30, image: AssetImage(kIconSeries)),
            const Spacer(),
            showSearch
                ? Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: TextField(
                        controller: _search,
                        onChanged: widget.onSearch,
                        decoration: const InputDecoration(
                          hintText: "Search...",
                        ),
                      ),
                    ),
                  )
                : const Spacer(),
            if (showSearch)
              IconButton(
                icon: const Icon(
                  FontAwesomeIcons.xmark,
                  color: Colors.white,
                ),
                focusColor: kColorPrimary,
                onPressed: () {
                  if (widget.onSearch != null) {
                    widget.onSearch!("");
                  }

                  setState(() {
                    showSearch = false;
                    _search.clear();
                  });
                },
              ),
            if (widget.onSearch != null)
              if (!showSearch)
                IconButton(
                  focusColor: kColorFocus,
                  onPressed: () {
                    setState(() {
                      showSearch = true;
                    });
                  },
                  icon: const Icon(
                    FontAwesomeIcons.magnifyingGlass,
                    color: Colors.white,
                  ),
                ),
            if (widget.onFavorite != null)
              IconButton(
                focusColor: kColorFocus,
                onPressed: widget.onFavorite,
                icon: Icon(
                  widget.isLiked
                      ? FontAwesomeIcons.solidHeart
                      : FontAwesomeIcons.heart,
                  color: Colors.white,
                ),
              ),
            IconButton(
              focusColor: kColorFocus,
              onPressed: () {
                context.read<VideoCubit>().changeUrlVideo(false);
                Get.back();
              },
              icon: const Icon(
                FontAwesomeIcons.chevronRight,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AppBarSettings extends StatelessWidget {
  const AppBarSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      color: Colors.transparent,
      //  margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(screenWidth(40)),
            child: Image(
              width: 0.3.dp,
              height: 0.3.dp,
              image: const AssetImage(kIconSplash),
            ),
          ),
          const SizedBox(width: 5),
          Text(
            "Settings",
            style: Get.textTheme.headline4,
          ),
          Container(
            width: 1,
            height: 8.h,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            color: kColorHint,
          ),
          Icon(
            FontAwesomeIcons.gear,
            size: 20.sp,
            color: Colors.white,
          ),
          const Spacer(),
          IconButton(
            focusColor: kColorFocus,
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              FontAwesomeIcons.chevronRight,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

class CardLiveItem extends StatelessWidget {
  const CardLiveItem(
      {Key? key,
      required this.title,
      required this.onTap,
      this.isSelected = false,
      this.link,
      this.image})
      : super(key: key);
  final String title;
  final Function() onTap;
  final bool isSelected;
  final String? link;
  final String? image;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      focusColor: kColorFocus,
      borderRadius: BorderRadius.circular(10),
      child: Ink(
        decoration: BoxDecoration(
          color: kColorPrimary.withOpacity(0.6),
          borderRadius: BorderRadius.circular(10),
          border: isSelected ? Border.all(color: Colors.yellow) : null,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: [
            image != null && !isSelected
                ? CachedNetworkImage(
                    imageUrl: image ?? "",
                    width: 9.w,
                    errorWidget: (_, i, e) {
                      return Icon(
                        FontAwesomeIcons.tv,
                        size: isSelected ? 18.sp : 16.sp,
                        color: Colors.white,
                      );
                    },
                  )
                : Icon(
                    isSelected ? FontAwesomeIcons.play : FontAwesomeIcons.tv,
                    size: isSelected ? 18.sp : 16.sp,
                    color: Colors.white,
                  ),
            const SizedBox(width: 13),
            Expanded(
              child: Text(
                title,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Get.textTheme.headline5!.copyWith(
                  color: Colors.white,
                ),
              ),
            ),
            if (isSelected && link != null)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 2),
                child: Icon(
                  FontAwesomeIcons.expand,
                  size: 18.sp,
                  color: isSelected ? Colors.yellow : null,
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class AppBarFav extends StatelessWidget {
  const AppBarFav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      color: Colors.transparent,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(screenWidth(40)),
            child: Image(
              width: 0.3.dp,
              height: 0.3.dp,
              image: const AssetImage(kIconSplash),
            ),
          ),
          const SizedBox(width: 5),
          Text(
            "Favourites",
            style: Get.textTheme.headline4,
          ),
          Container(
            width: 1,
            height: 8.h,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            color: kColorHint,
          ),
          Icon(
            FontAwesomeIcons.solidHeart,
            size: 20.sp,
            color: Colors.white,
          ),
          const Spacer(),
          IconButton(
            focusColor: kColorFocus,
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              FontAwesomeIcons.chevronRight,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

class AppBarCatchUp extends StatelessWidget {
  const AppBarCatchUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.w,
      color: Colors.transparent,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(screenWidth(40)),
            child: Image(
              width: 0.3.dp,
              height: 0.3.dp,
              image: const AssetImage(kIconSplash),
            ),
          ),
          const SizedBox(width: 5),
          Text(
            "Catch Up",
            style: Get.textTheme.headline4,
          ),
          Container(
            width: 1,
            height: 8.h,
            margin: const EdgeInsets.symmetric(horizontal: 13),
            color: kColorHint,
          ),
          Icon(
            FontAwesomeIcons.rotate,
            size: 20.sp,
            color: Colors.white,
          ),
          const Spacer(),
          IconButton(
            focusColor: kColorFocus,
            onPressed: () {
              context.read<WatchingCubit>().clearData();
            },
            icon: const Icon(
              FontAwesomeIcons.broom,
              color: Colors.white,
            ),
          ),
          IconButton(
            focusColor: kColorFocus,
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              FontAwesomeIcons.chevronRight,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

class CardEpgStream extends StatelessWidget {
  const CardEpgStream({Key? key, required this.streamId}) : super(key: key);
  final String? streamId;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: streamId == null
          ? const SizedBox()
          : FutureBuilder<List<EpgModel>>(
              future: IpTvApi.getEPGbyStreamId(streamId ?? ""),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: SizedBox(
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(),
                    ),
                  );
                } else if (!snapshot.hasData) {
                  return const SizedBox();
                }
                final list = snapshot.data;

                return Container(
                  decoration: const BoxDecoration(
                      color: kColorCardLight,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                      )),
                  margin: const EdgeInsets.only(top: 10),
                  child: ListView.separated(
                    itemCount: list!.length,
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 10,
                    ),
                    itemBuilder: (_, i) {
                      final model = list[i];
                      String description = String.fromCharCodes(
                          base64.decode(model.description ?? ""));
                      String title = String.fromCharCodes(
                          base64.decode(model.title ?? ""));
                      return CardEpg(
                        title:
                            "${getTimeFromDate(model.start ?? "")} - ${getTimeFromDate(model.end ?? "")} - $title",
                        description: description,
                        isSameTime: checkEpgTimeIsNow(
                            model.start ?? "", model.end ?? ""),
                      );
                    },
                    separatorBuilder: (_, i) {
                      return const SizedBox(
                        height: 10,
                      );
                    },
                  ),
                );
              }),
    );
  }
}

class CardEpg extends StatelessWidget {
  const CardEpg(
      {Key? key,
      required this.title,
      required this.description,
      required this.isSameTime})
      : super(key: key);
  final String title;
  final String description;
  final bool isSameTime;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Get.textTheme.bodyLarge!.copyWith(
            fontWeight: FontWeight.bold,
            fontSize: 15.sp,
            color: isSameTime ? kColorPrimaryDark : Colors.white,
          ),
        ),
        Text(
          description,
          style: Get.textTheme.bodyMedium!.copyWith(
            color: Colors.white70,
          ),
        ),
      ],
    );
  }
}

class CardChannelItemNew extends StatefulWidget {
  const CardChannelItemNew(
      {Key? key,
      required this.catySelected,
      required this.name,
      required this.onTap,
      required this.catyId,
      required this.onChannel,
      this.futureChannels,
      required this.selectedLive})
      : super(key: key);
  final bool catySelected;
  final String name;
  final String? catyId;
  final Future<List<ChannelLive>>? futureChannels;
  final ChannelLive? selectedLive;

  final Function() onTap;
  final Function(ChannelLive) onChannel;

  @override
  State<CardChannelItemNew> createState() => _CardChannelItemNewState();
}

class _CardChannelItemNewState extends State<CardChannelItemNew> {
  bool isOpen = true;
  @override
  Widget build(BuildContext context) {
    return Ink(
      width: double.infinity,
      child: SafeArea(
        left: true,
        right: true,
        top: false,
        bottom: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                widget.onTap();
                setState(() {
                  isOpen = !isOpen;
                });
                debugPrint("Click: $isOpen");
              },
              focusColor: kColorPrimaryDark,
              child: Ink(
                width: double.infinity,
                decoration: BoxDecoration(
                  // color: catySelected ? kColorPrimary : null,
                  border: widget.catySelected
                      ? const Border(
                          left: BorderSide(
                            color: kColorPrimaryDark,
                            width: 3,
                          ),
                        )
                      : null,
                ),
                padding: EdgeInsets.only(
                  top: 8,
                  bottom: 8,
                  left: widget.catySelected ? 10 : 0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.sp,
                      ),
                    ),
                    Icon(
                      !isOpen
                          ? FontAwesomeIcons.chevronUp
                          : FontAwesomeIcons.chevronDown,
                      size: 10,
                      color: Colors.white70,
                    ),
                  ],
                ),
              ),
            ),
            if (widget.catyId != null)
              FutureBuilder<List<ChannelLive>>(
                future: widget.futureChannels,
                builder: (_, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const LinearProgressIndicator();
                  } else if (snapshot.hasData) {
                    final channels = snapshot.data;

                    if (isOpen) {
                      return const SizedBox();
                    }

                    return ListView.builder(
                      itemCount: channels!.length,
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemBuilder: (_, i) {
                        final live = channels[i];

                        bool selected = widget.selectedLive == live;

                        return InkWell(
                          focusColor: kColorPrimaryDark,
                          onTap: () {
                            widget.onChannel(live);
                          },
                          child: Ink(
                            decoration: BoxDecoration(
                              border: selected
                                  ? const Border(
                                      left: BorderSide(
                                        color: kColorPrimary,
                                        width: 3,
                                      ),
                                    )
                                  : null,
                            ),
                            padding: const EdgeInsets.symmetric(
                              vertical: 5,
                              horizontal: 12,
                            ),
                            child: Row(
                              children: [
                                CachedNetworkImage(
                                  imageUrl: live.streamIcon ?? "",
                                  width: 28,
                                  height: 28,
                                  placeholder: (_, i) {
                                    return const CardNoImage();
                                  },
                                  errorWidget: (_, i, e) {
                                    return const CardNoImage();
                                  },
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: Text(
                                    live.name ?? "",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.sp,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  }

                  return const SizedBox();
                },
              ),
          ],
        ),
      ),
    );
  }
}
