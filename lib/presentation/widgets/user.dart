part of 'widgets.dart';

class CardInputLogin extends StatefulWidget {
  const CardInputLogin({
    Key? key,
    required this.hint,
    this.controller,
    this.onChange,
    this.autofocus = false,
    required this.textInputAction,
    required this.onSubmitted,
  }) : super(key: key);
  final String hint;
  final TextEditingController? controller;
  final Function(String)? onChange;
  final TextInputAction textInputAction;
  final bool autofocus;
  final Function(String) onSubmitted;

  @override
  State<CardInputLogin> createState() => _CardInputLoginState();
}

class _CardInputLoginState extends State<CardInputLogin> {
  bool isFocused = false;

  final FocusNode node = FocusNode();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // node.requestFocus();
        setState(() {
          isFocused = true;
        });
      },
      onFocusChange: (value) {
        if (value) {
          node.requestFocus();
        }
        setState(() {
          isFocused = value;
        });
      },
      child: Ink(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: isFocused ? kColorPrimary : Colors.white,
            width: 3,
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: TextField(
          focusNode: node,
          autofocus: widget.autofocus,
          onSubmitted: widget.onSubmitted,
          controller: widget.controller,
          onChanged: widget.onChange,
          textInputAction: widget.textInputAction,
          decoration: InputDecoration(
            hintText: widget.hint,
            hintStyle: Get.textTheme.subtitle2!.copyWith(
              color: Colors.grey,
            ),
            border: InputBorder.none,
          ),
          style: Get.textTheme.subtitle2!.copyWith(
            color: Colors.black,
          ),
          cursorColor: kColorPrimary,
        ),
      ),
    );
  }
}

class IntroImageAnimated extends StatefulWidget {
  const IntroImageAnimated({Key? key}) : super(key: key);

  @override
  State<IntroImageAnimated> createState() => _IntroImageAnimatedState();
}

class _IntroImageAnimatedState extends State<IntroImageAnimated> {
  late Timer timer;
  bool isImage = true;
  ScrollController controller = ScrollController();

  _startAnimation() async {
    const int second = 27;

    await Future.delayed(const Duration(milliseconds: 400));
    // debugPrint("start first one");

    await controller.animateTo(
      isImage ? controller.position.maxScrollExtent : 0,
      duration: const Duration(seconds: second),
      curve: Curves.linear,
    );

    if (mounted) {
      setState(() {
        isImage = !isImage;
      });
    }
    await _startAnimation();
  }

  @override
  void initState() {
    super.initState();
    _startAnimation();
  }

  @override
  void dispose() {
    //  timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        SizedBox(
          width: getSize(context).width,
          height: getSize(context).height,
          child: SingleChildScrollView(
            controller: controller,
            scrollDirection: Axis.horizontal,
            child: Image.asset(
              kImageIntro,
              fit: BoxFit.cover,
              width: getSize(context).width + 50,
              height: getSize(context).height,
            ),
          ),
        ),
      ],
    );
  }
}

class CardServerItem extends StatelessWidget {
  const CardServerItem(
      {Key? key,
      required this.title,
      required this.image,
      required this.onTap,
      required this.onHover,
      required this.isHover})
      : super(key: key);
  final String title;
  final String image;
  final Function() onTap;
  final Function(bool) onHover;

  final bool isHover;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      onHover: onHover,
      onFocusChange: onHover,
      borderRadius: BorderRadius.circular(8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Ink(
          decoration: const BoxDecoration(color: kColorBackDark, boxShadow: [
            BoxShadow(
              color: Colors.green,
              blurRadius: 3,
            )
          ]),
          child: Column(
            children: [
              Expanded(
                child: CachedNetworkImage(
                  imageUrl: image,
                  width: double.infinity,
                  height: double.infinity,
                  fit: BoxFit.cover,
                  placeholder: (_, i) {
                    return const CardNoImage();
                  },
                  errorWidget: (_, i, e) {
                    return const CardNoImage();
                  },
                ),
              ),
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: kColorPrimary, width: 4),
                  ),
                ),
                padding: const EdgeInsets.all(10),
                alignment: Alignment.bottomLeft,
                child: Text(
                  title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Get.textTheme.displayMedium!.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 13.sp,
                  ),
                ),
              ),
              if (isHover)
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: kColorPrimary,
                      width: 4,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
