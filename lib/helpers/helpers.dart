import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:universal_platform/universal_platform.dart';

part 'colors.dart';
part 'constants.dart';
part 'date_format.dart';
part 'functions.dart';
part 'routes.dart';
part 'themes.dart';
part 'toast.dart';

Size getSize(BuildContext context) => MediaQuery.of(context).size;
