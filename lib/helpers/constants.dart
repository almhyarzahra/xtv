part of 'helpers.dart';

const String kAppName = "xtv";

//TODO: SHow Ads ( true / false )
const bool showAds = false;

const String kIconLive = "assets/images/live-stream.png";
const String kIconSeries = "assets/images/clapperboard.png";
const String kIconMovies = "assets/images/film-reel.png";
const String kIconSplash = "assets/images/icon1.png";
const String kImageIntro = "assets/images/intro h.jpeg";
const String kImageLogo = "assets/images/icon1.png";

const String kPrivacy = "https://www.whmcssmarters.com/terms-of-service/";
const String kContact = "https://reseller.su";

double sizeTablet = 800; // 950;

enum TypeCategory {
  all,
  live,
  movies,
  series,
}

bool isTv(BuildContext context) {
  return MediaQuery.of(context).size.width > sizeTablet;
  // return UniversalPlatform.isDesktopOrWeb;
}
